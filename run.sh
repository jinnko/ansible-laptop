#!/usr/bin/env sh

# If you don't have howdy installed then you'll probably want to add --ask-become-pass
ansible-playbook --connection=local laptop.yml
